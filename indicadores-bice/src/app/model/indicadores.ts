import { Indicador } from './indicador';

export interface Indicadores {
    cobre: Indicador,
    dolar: Indicador,
    euro: Indicador,
    ipc: Indicador,
    ivp: Indicador,
    oro: Indicador,
    plata: Indicador,
    uf: Indicador,
    utm: Indicador,
    yen: Indicador
}