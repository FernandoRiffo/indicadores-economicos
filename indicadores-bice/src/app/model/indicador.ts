export interface Indicador {
    date: Date,
    key: string,
    name: string,
    unit: string,
    value: Number
}