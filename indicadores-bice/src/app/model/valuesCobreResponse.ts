import { Value } from './value';

export interface ValueCobreResponse {
    key: string,
    name: string,
    unit: string,
    values: Value[]
}