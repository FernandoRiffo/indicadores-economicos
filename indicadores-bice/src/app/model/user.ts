export interface User {
    from: string,
    to: string,
    name: string,
    subject: string,
    content: string
}