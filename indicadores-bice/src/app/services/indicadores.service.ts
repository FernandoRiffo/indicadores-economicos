import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IndicadoresService {

  constructor(private http: HttpClient) { }

  getLastValues(): Observable<any> {
    const url = environment.API + environment.BASE_URL + '/lastvalues';
    return this.http.get(url);
  }

  getPrevioIndicator(indicador, date): Observable<any> {
    // let re = /\//gi;
    // date = date.replace(re,'-')
    const url = environment.API + environment.BASE_URL + '/previous/' + indicador + '/' + date;
    return this.http.get(url);
  }
}
