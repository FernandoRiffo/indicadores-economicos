import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CobreService {

  constructor(private http: HttpClient) { }

  getValuesCobre(): Observable<any> {
    const url = environment.API + environment.BASE_URL + '/valuescobre';
    return this.http.get(url);
  }


}
