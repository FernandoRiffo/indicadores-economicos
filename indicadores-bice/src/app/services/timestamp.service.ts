import { Injectable } from '@angular/core';
import * as moment from 'moment-timezone';
import { Timestamp } from '../model/timestamp';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class TimestampService {
  private currentTS = new BehaviorSubject<Timestamp>(null);

  constructor() {
    moment.locale('es');
  }

  private setCurrent(): Timestamp {
    return this.buildTime(
      moment()
        .tz('UTC')
        .unix()
    );
  }

  private buildTime(timestamp) {
    const current = moment()
      .set(timestamp)
      .tz(moment.tz.guess());
    const utc = moment()
      .set(timestamp)
      .tz('UTC');
    return {
      current: this.getTimeObj(current),
      utc: this.getTimeObj(utc)
    };
  }

  private getTimeObj(momentObj) {
    return {
      tz: momentObj.tz(),
      current: momentObj.unix(),
      fullDate: momentObj.format('DD-MM-YYYY'),
      fullDateTime: momentObj.format('DD-MM-YYYY'),
      iso8601: momentObj.format()
    };
  }

  getCurrentTime(): Observable<Timestamp> {
    return this.currentTS.asObservable();
  }

  getConvertedUnixTime(timestamp): Timestamp {
    const utc = moment.unix(timestamp)
      .tz('UTC');
    const current = utc
      .clone()
      .tz(moment.tz.guess());
    return {
      current: this.getTimeObj(current),
      utc: this.getTimeObj(utc)
    };
  }

  getPreviousDay(timestamp, days){
    let previou = moment(timestamp, 'DD-MM-YYYY').subtract(days,'days').format();
    return previou
  }
}
