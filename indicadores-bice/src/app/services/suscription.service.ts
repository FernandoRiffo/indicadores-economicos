import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class SuscriptionService {

  constructor(private http: HttpClient) { }

  suscribeUser(user:User): Observable<any> {
    const url = environment.API + environment.BASE_URL + '/sendemail';
    return this.http.post(url, user);
  }

  searchEmail(user:User){
    const url = environment.API + environment.BASE_URL + '/searchemail';
    return this.http.post(url, user);
  }
}
