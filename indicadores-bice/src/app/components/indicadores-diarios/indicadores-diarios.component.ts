import { Component, OnInit } from '@angular/core';
import { IndicadoresService } from 'src/app/services/indicadores.service';
import { TimestampService } from 'src/app/services/timestamp.service';
import { Indicador } from 'src/app/model/indicador';
import { Indicadores } from 'src/app/model/indicadores';

@Component({
  selector: 'app-indicadores-diarios',
  templateUrl: './indicadores-diarios.component.html',
  styleUrls: ['./indicadores-diarios.component.css']
})
export class IndicadoresDiariosComponent implements OnInit {

  indicadores;
  indicadoresAnt = [];
  variacionDolar;
  variacionEuro;
  variacionUF;
  variacionUTM;

  constructor(
    private indicadoresService: IndicadoresService,
    private ts: TimestampService
  ) { }
  ngOnInit() {
    this.getLastValues();
  }

  getLastValues() {
    this.indicadoresService.getLastValues().subscribe(data => {
      this.convertTimestampToDate(data);
      this.indicadores = data;
    });
  }

  convertTimestampToDate(data) {
    Object.keys(data).forEach(el => {
      data[el].date = this.ts.getConvertedUnixTime(data[el].date);
      let previous = this.ts.getPreviousDay(data[el].date.utc.fullDateTime, 1);
      this.getPrevioIndicator(data[el], previous);
    });
  }

  getPrevioIndicator(indicador, date) {
    this.indicadoresService.getPrevioIndicator(indicador.key, date).subscribe(ind => {
      this.indicadoresAnt.push(ind);
      this.getVariciones(indicador, ind);
    });
  }

  getVariciones(indicadores, indicador) {
    switch (indicadores.key) {
      case 'dolar': {
        if (indicadores.value !== null && indicador.value !== null) {
          if (indicadores.value > indicador.value || indicadores.value < indicador.value) {
            this.variacionDolar = indicadores.value - indicador.value;
          } else {
            this.variacionDolar = 0;
          }
        } else {
          this.variacionDolar = 0;
        }
        break;
      }
      case 'euro': {
        if (indicadores.value !== null && indicador.value !== null) {
          if (indicadores.value > indicador.value || indicadores.value < indicador.value) {
            this.variacionEuro = indicadores.value - indicador.value;
          } else {
            this.variacionEuro = 0;
          }
        } else {
          this.variacionEuro = 0;
        }
        break;
      }
      case 'uf': {
        if (indicadores.value !== null && indicador.value !== null) {
          if (indicadores.value > indicador.value || indicadores.value < indicador.value) {
            this.variacionUF = indicadores.value - indicador.value;
          } else {
            this.variacionUF = 0;
          }
        } else {
          this.variacionUF = 0;
        }
        break;
      }
      case 'utm': {
        if (indicadores.value !== null && indicador.value !== null) {
          if (indicadores.value > indicador.value || indicadores.value < indicador.value) {
            this.variacionUTM = indicadores.value - indicador.value;
          } else {
            this.variacionUTM = 0;
          }
        } else {
          this.variacionUTM = 0;
        }
        break;
      }
    }
  }
}
