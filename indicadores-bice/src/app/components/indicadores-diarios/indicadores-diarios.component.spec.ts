import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicadoresDiariosComponent } from './indicadores-diarios.component';

describe('IndicadoresDiariosComponent', () => {
  let component: IndicadoresDiariosComponent;
  let fixture: ComponentFixture<IndicadoresDiariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicadoresDiariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicadoresDiariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
