import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { User } from 'src/app/model/user';
import { SuscriptionService } from 'src/app/services/suscription.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-form-suscription',
  templateUrl: './form-suscription.component.html',
  styleUrls: ['./form-suscription.component.css']
})
export class FormSuscriptionComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private suscription: SuscriptionService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      console.log(this.registerForm);
      let user: User = {
        name: this.registerForm.value.name,
        subject: "Indicadores Economicos Diarios",
        to: this.registerForm.value.email,
        from: "fdo.riffo93@gmail.com",
        content: "Gracias por Suscribirte al envio de indicadores economicos"
      };
      this.searchEmail(user);
    }
  }

  get f() { return this.registerForm.controls; }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }

  suscribeUser(user: User) {
    this.suscription.suscribeUser(user).subscribe(user => {
      if (user) {
        this.toastr.success('Estimado ' + user.nombre + ' su suscribcion diaria ah sido exitosa!');
        this.onReset();
      } else {
        this.toastr.error('Estimado ' + user.nombre + ' ah ocurrido un error al realizar la suscripcion');
        this.onReset();
      }
    },
      error => {
        this.toastr.error('Error al procesar la solicitud');
        this.onReset();
      });
  }

  searchEmail(user: User) {
    this.suscription.searchEmail(user).subscribe(existe => {
      if (existe) {
        this.toastr.warning('Estimado el correo ' + user.to + ' Existe en nuestros registros!');
        this.onReset();
      } else {
        this.suscribeUser(user);
        this.onReset();
      }
    },
      error => {
        this.toastr.error('Error al procesar la solicitud');
        this.onReset();
      });
  }

}
