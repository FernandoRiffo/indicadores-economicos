import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSuscriptionComponent } from './form-suscription.component';

describe('FormSuscriptionComponent', () => {
  let component: FormSuscriptionComponent;
  let fixture: ComponentFixture<FormSuscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSuscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSuscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
