import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ValueCobreResponse } from 'src/app/model/valuesCobreResponse';
import { CobreService } from 'src/app/services/cobre.service';
import { TimestampService } from 'src/app/services/timestamp.service';
import { Fecha } from 'src/app/model/fecha';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-highcharts',
  templateUrl: './highcharts.component.html',
  styleUrls: ['./highcharts.component.css']
})
export class HighchartsComponent implements OnInit {

  valuesCobre: ValueCobreResponse;

  title;
  data;
  highcharts;
  chartOptions;
  valuesToChart: any[] = [];
  categorieToChart: any[] = [];
  valuesToChartToShow: any[] = [];
  categorieToChartToshow: any[] = [];
  fecha

  constructor(
    private cobreService: CobreService,
    private ts: TimestampService
  ) { }
  ngOnInit() {
    this.getValuesCobre();
  }

  procesarFecha(fecha) {
    this.fecha = fecha;
    if (this.valuesCobre) {
      this.filtrarData(fecha)
    }
  }

  getValuesCobre() {
    this.cobreService.getValuesCobre().subscribe(data => {
      this.valuesCobre = data;
      this.filtrarData(this.fecha);
    });
  }

  filtrarData(fecha: Fecha) {
    if(fecha){

      this.valuesToChart = [];
      this.categorieToChart = [];
      let dateDesde = moment(fecha.desde, "DD-MM-YYYY");
      let dateHasta = moment(fecha.hasta, "DD-MM-YYYY");
      for (const value of this.valuesCobre.values) {
        let date = moment.unix(value.date);
        if (date.isBetween(dateDesde, dateHasta)) {
          const chart = [value.value];
          this.fecha = date.format("DD-MM-YYYY");
          this.valuesToChart.push(chart);
          this.categorieToChart.push(this.fecha);
        }
      }
      this.setDataToChart();
    }
  }

  setDataToChart() {
    this.title = 'myHighchart';

    this.data = [{
      name: this.valuesCobre.name,
      data: this.valuesToChart
    }];

    this.highcharts = Highcharts;
    this.chartOptions = {
      chart: {
        type: "spline"
      },
      title: {
        text: this.valuesCobre.name
      },
      xAxis: {
        categories: this.categorieToChart
      },
      yAxis: {
        title: {
          text: "Dolar"
        }
      },
      series: this.data
    };
  }

}
