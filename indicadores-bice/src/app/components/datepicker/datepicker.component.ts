import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment-timezone';
import { Fecha } from 'src/app/model/fecha';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css']
})
export class DatepickerComponent implements OnInit {


  hoveredDate: NgbDate | null = null;
  @Output()
  fecha = new EventEmitter<any>();
  fromDate;
  toDate;

  constructor(
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter,
    private toastr: ToastrService
  ) {
    this.toDate = calendar.getToday();
    this.fromDate = calendar.getPrev(calendar.getToday(), 'm', 6);
  }
  ngOnInit() {
    this.enviarFecha();
  }

  onDateSelection(date: NgbDate) {
    if (this.toDate) {
      let dateDesde = moment(this.format(date), "DD-MM-YYYY");
      let dateHasta = moment(this.format(this.toDate), "DD-MM-YYYY");
      if (dateDesde.isBefore(dateHasta)) {
        this.fromDate = date;
        this.enviarFecha();
      } else {
        this.toastr.warning('La fecha Desde debe ser anterior a la fecha Hasta');
        this.fromDate = null;
      }
    } else {
      this.fromDate = date;
    }
  }

  onDateSelectionHasta(date: NgbDate) {
    if (this.fromDate) {
      let dateDesde = moment(this.format(this.fromDate), "DD-MM-YYYY");
      let dateHasta = moment(this.format(date), "DD-MM-YYYY");
      if (dateDesde.isBefore(dateHasta)) {
        this.toDate = date;
        this.enviarFecha();
      } else {
        this.toastr.warning('La fecha Hasta debe ser posterior a la fecha Desde');
        this.toDate = null;
      }
    } else {
      this.toastr.warning('Seleccione Fecha desde');
      this.toDate = null;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  enviarFecha() {
    let date: Fecha = {
      desde: this.fromDate.day + '-' + this.fromDate.month + '-' + this.fromDate.year,
      hasta: this.toDate.day + '-' + this.toDate.month + '-' + this.toDate.year
    }
    this.fecha.emit(date);
  }

  format(date: NgbDateStruct): string {
    if (!date) return '';
    let mdt = moment([date.year, date.month - 1, date.day]);
    if (!mdt.isValid()) return '';
    return mdt.format("DD-MM-YYYY");
  }

}
