import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CobreService } from './services/cobre.service';
import { HighchartsComponent } from './components/highcharts/highcharts.component';

import { HighchartsChartModule } from 'highcharts-angular';
import { HttpClientModule } from '@angular/common/http';
import { FormSuscriptionComponent } from './components/form-suscription/form-suscription.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { HeaderComponent } from './components/header/header.component';
import { IndicadoresDiariosComponent } from './components/indicadores-diarios/indicadores-diarios.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ToastrModule } from 'ngx-toastr';
import { TimestampService } from './services/timestamp.service';
import { DatepickerComponent } from './components/datepicker/datepicker.component';





@NgModule({
  declarations: [
    AppComponent,
    HighchartsComponent,
    FormSuscriptionComponent,
    DashboardComponent,
    HeaderComponent,
    IndicadoresDiariosComponent,
    DatepickerComponent
  ],
  imports: [
    HighchartsChartModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    NgbModule,
    NgbAlertModule,
    MDBBootstrapModule.forRoot(),
    ToastrModule.forRoot()

  ],
  providers: [
    CobreService,
    TimestampService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
