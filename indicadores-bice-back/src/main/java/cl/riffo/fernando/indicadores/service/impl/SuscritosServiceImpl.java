package cl.riffo.fernando.indicadores.service.impl;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.riffo.fernando.indicadores.model.Users;
import cl.riffo.fernando.indicadores.repository.SuscritosRepository;
import cl.riffo.fernando.indicadores.service.SuscritosService;

@Service
public class SuscritosServiceImpl implements SuscritosService{
	
	@Autowired private SuscritosRepository suscritosRepository;
	
	@Override
	public List<Users> findAll() throws MessagingException, IOException{
		return this.suscritosRepository.findAll();
	}
	
	@Override
	public Users save(Users user) throws MessagingException, IOException{
		return this.suscritosRepository.save(user);
	}
	
	@Override
	public Users search(Users user) throws MessagingException, IOException{
		return this.suscritosRepository.findByto(user.getMail());
	}

}
