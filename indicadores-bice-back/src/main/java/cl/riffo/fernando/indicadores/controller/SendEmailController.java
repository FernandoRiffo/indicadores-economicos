package cl.riffo.fernando.indicadores.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.riffo.fernando.indicadores.model.MailModel;
import cl.riffo.fernando.indicadores.model.Users;
import cl.riffo.fernando.indicadores.service.SendEmailService;
import cl.riffo.fernando.indicadores.service.SuscritosService;
import freemarker.template.TemplateException;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class SendEmailController {

	private static Logger log = LoggerFactory.getLogger(SendEmailController.class);

	@Autowired
	private SendEmailService sendEmailService;

	@Autowired
	private SuscritosService suscritosService;

	@PostMapping("/sendemail")
	public ResponseEntity<?> sendEmail(@RequestBody MailModel mailModel) {

		log.info("Sending Email...");

		Users usuario = new Users();
		usuario.setNombre(mailModel.getName());
		usuario.setMail(mailModel.getTo());
		usuario.setSuscrito(true);

		try {
			this.sendEmailService.sendEmail(usuario);
			log.info("Close send email...");
			return ResponseEntity.ok().body(usuario);

		} catch (MessagingException | IOException e) {
			return ResponseEntity.ok().body(e.getMessage());
		}

	}

	@PostMapping("/sendemailwithattachment")
	public ResponseEntity<?> sendemailwithattachment(@RequestBody MailModel mailModel) {
		try {
			sendEmailService.sendEmailNew(mailModel);
			return ResponseEntity.ok().body(mailModel.toString());
		} catch (MessagingException | IOException | TemplateException e) {
			return ResponseEntity.ok().body(e.getMessage());
		}

	}

	@PostMapping("/searchemail")
	public boolean search(@RequestBody MailModel mailModel) {

		log.info("Search Email...");

		boolean existe = false;

		Users usuario = new Users();
		usuario.setNombre(mailModel.getName());
		usuario.setMail(mailModel.getTo());
		usuario.setSuscrito(true);

		try {
			existe = this.sendEmailService.search(usuario);

		} catch (MessagingException | IOException e) {
			log.error(e.getMessage());
		}
		return existe;

	}

	@Scheduled(cron = "${date.crontab.ejecution}")
	public void scheduleTaskToSendEmail() {

		List<Users> listSuscritos = new ArrayList<>();

		try {
			log.info("Search suscritos...");
			listSuscritos = suscritosService.findAll();
			sendEmailSuscritos(listSuscritos);

		} catch (MessagingException | IOException e) {
			log.error(e.getMessage());
		}

	}

	public void sendEmailSuscritos(List<Users> suscritos) {
		for (Users suscrito : suscritos) {
			if (suscrito.isSuscrito()) {

				MailModel newReport = new MailModel();

				newReport.setFrom("fdo.riffo93@gmail.com");
				newReport.setTo(suscrito.getMail());
				newReport.setSubject("Indicadores Financieros Diarios");
				newReport.setName(suscrito.getNombre());
				newReport.setContent("");

				try {
					sendEmailService.sendEmailNew(newReport);
				} catch (MessagingException | IOException | TemplateException e) {
					log.error(e.getMessage());
				}
			}
		}
		log.info("Sending Email end...");
	}

}
