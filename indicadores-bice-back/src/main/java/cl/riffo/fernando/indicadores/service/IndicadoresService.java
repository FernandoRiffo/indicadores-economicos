package cl.riffo.fernando.indicadores.service;

import org.springframework.http.ResponseEntity;

public interface IndicadoresService {

	Object getValuesCobre() throws Exception;

	ResponseEntity<Object> getLastValues() throws Exception;

	Object getPreviousIndicator(String indicador, String date) throws Exception;
}
