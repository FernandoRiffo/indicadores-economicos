package cl.riffo.fernando.indicadores.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Response {
	
	private String key;
	private String name;
	private String unit;
	private Date date;
	private Double value;
	
	
}
