package cl.riffo.fernando.indicadores;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class IndicadoresBiceBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndicadoresBiceBackApplication.class, args);
	}
	
}
