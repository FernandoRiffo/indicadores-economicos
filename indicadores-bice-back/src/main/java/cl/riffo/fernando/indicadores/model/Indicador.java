package cl.riffo.fernando.indicadores.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Indicador {

	private String key;
	private String name;
	private String unit;
	private long date;
	private double value;
}
