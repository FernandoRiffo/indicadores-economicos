package cl.riffo.fernando.indicadores.service;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import cl.riffo.fernando.indicadores.model.Users;

public interface SuscritosService {

	List<Users> findAll() throws MessagingException, IOException;

	Users save(Users user) throws MessagingException, IOException;

	Users search(Users user) throws MessagingException, IOException;

}
