package cl.riffo.fernando.indicadores.service;

import java.io.IOException;

import javax.mail.MessagingException;

import cl.riffo.fernando.indicadores.model.MailModel;
import cl.riffo.fernando.indicadores.model.Users;
import freemarker.template.TemplateException;

public interface SendEmailService {
	
	void sendEmail(Users user) throws MessagingException, IOException;
	
    void sendEmailNew(MailModel mailModel) throws MessagingException, IOException, TemplateException;

	boolean search(Users user) throws MessagingException, IOException;


}
