package cl.riffo.fernando.indicadores.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@ToString
public class DashboardCobreResponse {
	
	private String key;
	private String name;
	private String unit;
	private List<Values> values;

}
