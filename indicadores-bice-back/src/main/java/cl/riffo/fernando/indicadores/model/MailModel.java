package cl.riffo.fernando.indicadores.model;

import java.util.Map;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
@ToString
@Getter
@Setter
public class MailModel {

	private String from;
    private String to;
    private String name;
    private String subject;
    private String content;
    private Map<String, Object> model;
}
