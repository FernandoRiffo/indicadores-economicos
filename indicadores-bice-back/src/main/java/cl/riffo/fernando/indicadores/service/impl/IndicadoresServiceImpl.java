package cl.riffo.fernando.indicadores.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cl.riffo.fernando.indicadores.model.DashboarCobre;
import cl.riffo.fernando.indicadores.model.DashboardCobreResponse;
import cl.riffo.fernando.indicadores.model.Values;
import cl.riffo.fernando.indicadores.service.IndicadoresService;

@Service
public class IndicadoresServiceImpl implements IndicadoresService {

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	private static Logger log = LoggerFactory.getLogger(IndicadoresServiceImpl.class);

	@Override
	public Object getValuesCobre() throws Exception {

		ResponseEntity<DashboarCobre> response = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("user-agent",
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
			HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

			response = restTemplate.exchange("https://www.indecon.online/values/cobre", HttpMethod.GET, entity,
					DashboarCobre.class);

			DashboardCobreResponse cobreValue = new DashboardCobreResponse();
			cobreValue.setKey(response.getBody().getKey());
			cobreValue.setName(response.getBody().getName());
			cobreValue.setUnit(response.getBody().getUnit());

			List<Values> values = new ArrayList<>();

			for (Map.Entry m : response.getBody().getValues().entrySet()) {

				Values value = new Values();
				value.setDate(m.getKey());
				value.setValue(m.getValue());

				values.add(value);

			}

			cobreValue.setValues(values);

			return cobreValue;

		} catch (Exception ex) {
			log.error(ex.getMessage());

		}

		return response;

	}

	@Override
	public ResponseEntity<Object> getLastValues() throws Exception {

		ResponseEntity<Object> response = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("user-agent",
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
			HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

			response = restTemplate.exchange("https://www.indecon.online/last", HttpMethod.GET, entity, Object.class);

		} catch (Exception ex) {
			log.error(ex.getMessage());

		}

		return response;

	}

	@Override
	public Object getPreviousIndicator(String indicador, String date) throws Exception {
		Object response = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("user-agent",
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
			HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

			response = restTemplate.exchange("https://www.indecon.online/date/" + indicador + "/" + date,
					HttpMethod.GET, entity, Object.class);

		} catch (Exception ex) {
			log.error(ex.getMessage());

		}

		return response;
	}

}
