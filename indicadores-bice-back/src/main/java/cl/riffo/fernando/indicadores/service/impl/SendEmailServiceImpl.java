package cl.riffo.fernando.indicadores.service.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cl.riffo.fernando.indicadores.model.Indicadores;
import cl.riffo.fernando.indicadores.model.MailModel;
import cl.riffo.fernando.indicadores.model.Users;
import cl.riffo.fernando.indicadores.service.IndicadoresService;
import cl.riffo.fernando.indicadores.service.SendEmailService;
import cl.riffo.fernando.indicadores.service.SuscritosService;
import freemarker.template.Configuration;
import freemarker.template.Template;

@Service
public class SendEmailServiceImpl implements SendEmailService {

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	private SuscritosService suscritosService;

	@Autowired
	private IndicadoresService indicadoresService;

	@Value("${url.servicio}")
	private String url;

	@Autowired
	@Qualifier("emailConfigBean")
	private Configuration emailConfig;

	private static Logger log = LoggerFactory.getLogger(SendEmailServiceImpl.class);

	@Override
	public void sendEmail(Users user) throws MessagingException, IOException {

		try {
			this.suscritosService.save(user);

			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setTo(user.getMail());

			msg.setSubject("Confirmación de suscripción Indicadores Diarios");
			msg.setText("Gracias por su preferencia");

			javaMailSender.send(msg);
		} catch (Exception e) {
			log.error(e.getMessage());
		}

	}

	@Override
	public void sendEmailNew(MailModel mailModel) {

		try {

			ResponseEntity<Object> indicadoresResponse = indicadoresService.getLastValues();
			indicadoresResponse.getBody();
			LinkedHashMap<String, Object> indicadores = (LinkedHashMap<String, Object>) indicadoresResponse.getBody();
			Iterator it = indicadores.entrySet().iterator();
			Map<String, Object> model = new HashMap<>();
			while (it.hasNext()) {
				Indicadores in = getIndicador(it.next());
				if (in.getDolar() != null) {
					model.put("dolarDate", getDate(in.getDolar().getDate()));
					model.put("dolarValue", in.getDolar().getValue());
				} else if (in.getEuro() != null) {
					model.put("euroDate", getDate(in.getEuro().getDate()));
					model.put("euroValue", in.getEuro().getValue());
				} else if (in.getUtm() != null) {
					model.put("utmDate", getDate(in.getUtm().getDate()));
					model.put("utmValue", in.getUtm().getValue());
				} else if (in.getUf() != null) {
					model.put("ufDate", getDate(in.getUf().getDate()));
					model.put("ufValue", in.getUf().getValue());
				}
			}

			model.put("name", mailModel.getName());
			model.put("location", "Fernando Riffo");
			model.put("signature", "www.linkedin.com/in/fernando-riffo-matamala");
			model.put("content", mailModel.getContent());
			model.put("url", url);

			mailModel.setModel(model);

			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message,
					MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			mimeMessageHelper.addInline("logo.png", new ClassPathResource("classpath:images/logo.png"));

			Template template = emailConfig.getTemplate("email.ftl");
			String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, mailModel.getModel());

			mimeMessageHelper.setTo(mailModel.getTo());
			mimeMessageHelper.setText(html, true);
			mimeMessageHelper.setSubject(mailModel.getSubject());
			mimeMessageHelper.setFrom(mailModel.getFrom());

			emailSender.send(message);
		} catch (Exception e) {
			log.error(e.getMessage());
		}

	}

	@Override
	public boolean search(Users user) throws MessagingException, IOException {

		boolean existe = false;
		Users busqueda = new Users();

		try {
			busqueda = this.suscritosService.search(user);
			if (busqueda != null) {
				log.info("Close search email...");
				existe = true;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return existe;
	}

	public Indicadores getIndicador(Object object) throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
		return new ObjectMapper().readValue(json, Indicadores.class);

	}

	public String getDate(long timestamp) {
		Calendar mydate = Calendar.getInstance();
		mydate.setTimeInMillis(timestamp * 1000);
		return mydate.get(Calendar.DAY_OF_MONTH) + "-" + mydate.get(Calendar.MONTH) + "-"
				+ mydate.get(Calendar.YEAR);
		
	}

}
