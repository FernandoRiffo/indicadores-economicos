package cl.riffo.fernando.indicadores.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Values {
	
	private Object date;
	private Object value;

}
