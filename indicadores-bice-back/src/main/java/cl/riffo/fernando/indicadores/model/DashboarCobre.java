package cl.riffo.fernando.indicadores.model;

import java.util.LinkedHashMap;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DashboarCobre {

	private String key;
	private String name;
	private String unit;
	private LinkedHashMap<String,Double> values;

}
