package cl.riffo.fernando.indicadores.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.riffo.fernando.indicadores.service.IndicadoresService;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class IndicadoresController {
	
	@Autowired
	private IndicadoresService indicadoresService;
	
	@GetMapping("valuescobre")
	public Object getValuesCobre() throws Exception {
		
		return this.indicadoresService.getValuesCobre();
	}
	
	@GetMapping("lastvalues")
	public ResponseEntity<Object> getLastValues() throws Exception {
		
		return this.indicadoresService.getLastValues();
	}
	
	@GetMapping("previous/{indicador}/{date}")
	public Object getPreviousIndicator(@PathVariable String indicador, @PathVariable String date) throws Exception {
		
		return this.indicadoresService.getPreviousIndicator(indicador, date);
	}

}
