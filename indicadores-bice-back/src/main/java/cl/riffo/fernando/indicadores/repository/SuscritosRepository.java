package cl.riffo.fernando.indicadores.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cl.riffo.fernando.indicadores.model.Users;

@Repository
public interface SuscritosRepository extends JpaRepository<Users, Long>{

	@Query("select s from Users s where s.mail = (?1)")
	Users findByto(String emailTo);

}
