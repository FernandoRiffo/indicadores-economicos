package cl.riffo.fernando.indicadores.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Indicadores {
	private Indicador dolar;
	private Indicador euro;
	private Indicador uf;
	private Indicador utm;
	private Indicador ipc;
	private Indicador ivp;
	private Indicador yen;
	private Indicador oro;
	private Indicador cobre;
	private Indicador plata;
}
