<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//ES" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>Sending Email with Freemarker HTML Template Example</title>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


  <!-- use the font -->
  <style>
    body {
      font-family: 'Roboto', sans-serif;
      font-size: 48px;
    }
  </style>
</head>

<body style="margin: 0; padding: 0;">

  <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
    <tr>
      <td align="center" style="padding: 40px 0 30px 0; display:flex;" bgcolor="#eaeaea">
          <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Valor Dolar</h5>
              <h6 class="card-subtitle mb-2 text-muted">${dolarDate}</h6>
              <h6 class="card-subtitle mb-2 text-muted">${dolarValue}</h6>
            </div>
          </div>
          <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Valor Euro</h5>
              <h6 class="card-subtitle mb-2 text-muted">${euroDate}</h6>
              <h6 class="card-subtitle mb-2 text-muted">${euroValue}</h6>
            </div>
          </div>
          <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Valor UF</h5>
              <h6 class="card-subtitle mb-2 text-muted">${ufDate}</h6>
              <h6 class="card-subtitle mb-2 text-muted">${ufValue}</h6>
            </div>
          </div>
          <div class="card" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">Valor UTM</h5>
              <h6 class="card-subtitle mb-2 text-muted">${utmDate}</h6>
              <h6 class="card-subtitle mb-2 text-muted">${utmValue}</h6>
            </div>
          </div>
      </td>
    </tr>
    <tr>
      <td bgcolor="#eaeaea" style="padding: 40px 30px 40px 30px;">
        <h5>Estimado ${name},</h5>
        <h5>${content}</h5>
        <h5>Puedes revisar más información en la siguente página</h5>
        <a href="${url}" class="btn btn-primary">Indicadores BICE</a>
      </td>
    </tr>
    <tr>
      <td bgcolor="#777777" style="padding: 30px 30px 30px 30px;">
        <h5>${location}</h5>
        <h5>${signature}</h5>
      </td>
    </tr>
  </table>

</body>

</html>