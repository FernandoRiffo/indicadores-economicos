package cl.riffo.fernando.indicadores.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import cl.riffo.fernando.indicadores.model.Users;
import cl.riffo.fernando.indicadores.repository.SuscritosRepository;
import cl.riffo.fernando.indicadores.service.SuscritosService;
import lombok.extern.slf4j.Slf4j;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

@Slf4j
public class SuscritosServiceTest {
	
    /** The Suscritos repository. */
    @Mock
    private SuscritosRepository suscritosRepository;
    
    @InjectMocks
    private SuscritosService suscritosService = new SuscritosServiceImpl();
    
    PodamFactory factory = new PodamFactoryImpl();
    
    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

	@Test
    public void search() throws Exception {

        log.debug("########### Ini search() ###########");
        
        Users user = factory.manufacturePojo(Users.class);
        
        Mockito.doReturn(user).when(suscritosRepository).findByto("nathox93@gmail.com");
        
        Users userSearch = new Users();
        userSearch.setIdUser(1L);
        userSearch.setNombre("perico");
        userSearch.setMail("nathox93@gmail.com");
        userSearch.setSuscrito(true);

        Users output= suscritosService.search(userSearch);
        System.out.println(output);
        
        assertNotNull(output);

        log.debug("########### Fin search() ###########");
    }
	
	@Test
    public void findAll() throws Exception {

        log.debug("########### Ini findAll() ###########");
        
        List<Users> listUser = new ArrayList<>();
        
        Mockito.doReturn(listUser).when(suscritosRepository).findAll();
        

        List<Users> output= suscritosService.findAll();
        System.out.println(output);
        
        assertNotNull(output);

        log.debug("########### Fin findAll() ###########");
    }
	
	@Test
    public void save() throws Exception {

        log.debug("########### Ini save() ###########");
        
        Users user = factory.manufacturePojo(Users.class);
       
        Users output= suscritosService.save(user);
        System.out.println(output);
        assertNull(output);

        log.debug("########### Fin save() ###########");
    }
}
