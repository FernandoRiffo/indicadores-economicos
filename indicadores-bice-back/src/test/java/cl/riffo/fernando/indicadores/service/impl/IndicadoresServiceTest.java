package cl.riffo.fernando.indicadores.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.web.client.RestTemplate;

import cl.riffo.fernando.indicadores.service.IndicadoresService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IndicadoresServiceTest {
	
	@Mock
    private RestTemplate restTemplate;
 
    @InjectMocks
    private IndicadoresService indicadoresService = new IndicadoresServiceImpl();
     
    @Test
    public void getLastValues() throws Exception {
		
    	log.debug("########### Ini getLastValues() ###########");
    	Object valueCobre = indicadoresService.getLastValues();
        assertNotNull(valueCobre);
        log.debug("########### Ini getLastValues() ###########");
    }
    
    @Test
    public void getValuesCobre() throws Exception {
		
    	log.debug("########### Ini getValuesCobre() ###########");
    	Object indicadores = indicadoresService.getValuesCobre();
        assertNotNull(indicadores);
        log.debug("########### Ini getValuesCobre() ###########");
    }
    
    @Test
    public void getPreviousIndicator() throws Exception {
		
    	log.debug("########### Ini getPreviousIndicator() ###########");
    	Object indicadores = indicadoresService.getPreviousIndicator("","");
        assertNull(indicadores);
        log.debug("########### Ini getPreviousIndicator() ###########");
    }

}
